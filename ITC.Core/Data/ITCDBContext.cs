﻿using System;
using Microsoft.EntityFrameworkCore;
using ITC.Data.Entities;
using System.Reflection;

namespace ITC.Core.Data
{
	public class ITCDBContext : DbContext
	{
        public ITCDBContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);


            modelBuilder.Entity<Staff>()
                .HasOne(a => a.Leader)
                .WithOne(b => b.Staff)
                .HasForeignKey<Leader>(b => b.Id);

              modelBuilder.Entity<EmpTasks>()
                .HasOne(a => a.Staffs)
                .WithOne(b => b.EmpTasks)
                .HasForeignKey<Staff>(b => b.Id);    
        }
        #region DB Set
        public DbSet<Avatar> Avatars { get; set; }
        public DbSet<BankInfo> BankInfos { get; set; }
        public DbSet<Campus> Campuses { get; set; }
        public DbSet<Comment> Comments { get; set; } 
        public DbSet<CompCriteria> CompCriterias { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<EmpTasks> EmpTasks { get; set; }
        public DbSet<GradeReport> GradeReports { get; set; }
        public DbSet<History> Histories { get; set; }
        public DbSet<JoinProject> JoinProjects { get; set; }
        public DbSet<Leader> Leaders { get; set; }
        public DbSet<Major> Majors { get; set; }
        public DbSet<Noti> Notis { get; set; }
        public DbSet<Partner> Partners { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Registration> Registrations { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<RoleUser> RoleUsers { get; set; }
        public DbSet<Slot> Slots { get; set; }
        public DbSet<Staff> Staffs { get; set; }
        public DbSet<Status> Statuses { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<StudentJoinProject> StudentJoinProjects { get; set; }
        public DbSet<Syllabus> Syllabus { get; set; }

        #endregion
    }
}

