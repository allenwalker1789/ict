﻿using System;
using ITC.Data.Entities;

namespace ITC.Core.Interface
{
    public interface ILoginService
    {
        Staff AuthenticateUser(string userName, string password);
    }
}

