﻿using System;
using ITC.Data.Entities;
using System.Security.Claims;

namespace ITC.Core.Interface
{
	public interface IJwtTokenService
	{
        string GenerateTokenUser(Staff account);
        Task<string> GenerateTokenDMSAsync(Staff account);
        string GenerateToken(params Claim[] claims);
    }
}

