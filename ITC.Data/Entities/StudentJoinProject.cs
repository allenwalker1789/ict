﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ITC.Data.Entities
{
    public class StudentJoinProject
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [ForeignKey("ProjectId")]
        public Guid PrjId { get; set; }

        [ForeignKey("StudentId")]
        public Guid StId { get; set; }

        public virtual Project? Project { get; set; }
        public virtual Student? Student{ get; set; }
    }
}
