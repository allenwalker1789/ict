﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ITC.Data.Entities
{
    public class Registration
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? PhoneNumber { get; set; }
        public string? Address { get; set; }
        public string? PassportNo { get; set; }
        public string? PassportImg { get; set; }
        public string? FbUrl { get; set; }
        public DateTime BirthDay { get; set; }
        public DateTime SchoolYear { get; set; }
        public bool StatusAcceptOfStudent { get; set; }


        [ForeignKey("CourseId")]
        public Guid CourseId { get; set; }

        public virtual Course? Course { get; set; }
        public ICollection<Student>? Students { get; set; }
    }
}
