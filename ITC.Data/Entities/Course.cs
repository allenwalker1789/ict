﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ITC.Data.Entities
{
    public class Course
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string? Activity { get; set; }
        public string? Content { get; set; }
        public string? SkillName { get; set; }
        public bool IsActive { get; set; }

        [ForeignKey("CompCriteriaId")]
        public Guid CompCriteriaId { get; set; }

        public virtual CompCriteria? CompCriteria { get; set; }
        public ICollection<Campus>? Campus { get; set; }
        public ICollection<Slot>? Slots{ get; set; }
        public ICollection<Syllabus>? Syllabus { get; set; }
        public ICollection<GradeReport>? GradeReports { get; set; }
        public ICollection<Registration>? Registrations { get; set; }
    }
}
