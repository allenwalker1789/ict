﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ITC.Data.Entities
{
    public class Post
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string? Author { get; set; }
        public string? Title { get; set; }
        public DateTime PostDate  { get; set; }
        public bool Status { get; set; }

        [ForeignKey("StaffId")]
        public Guid StfId { get; set; }

        public virtual Staff? Staffs { get; set; }
    }
}
