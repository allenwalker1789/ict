﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.Data.Entities
{
    public class Partner
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string? Name { get; set; }
        public string? Contact { get; set; }
        public string? Feature { get; set; }
        public string? Note { get; set; }

        [ForeignKey("ProjectId")]
        public Guid PrjId { get; set; }

        public virtual Project? Project { get; set; }
    }
}
