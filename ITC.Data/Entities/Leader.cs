﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.Data.Entities
{
    public class Leader
    {
        [Key]
        public Guid Id { get; set; }
        public DateTime AssignDate { get; set; }
        public bool IsActive { get; set; }

        public virtual Staff? Staff { get; set; }
        public ICollection<Project>? Projects { get; set; }

    }
}
