﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ITC.Data.Entities
{
	public class Staff
	{
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]		
        public Guid Id { get; set; }
        public string? Username { get; set; }
        public string? Password { get; set; }
        public string? FullName { get; set; }
        public string? StaffNo { get; set; }
        public DateTime BirthDay { get; set; }
        public string? PhoneNumber { get; set; }
        public string? Email { get; set; }
        public string? Address { get; set; }
        public string? Gender { get; set; }
        public bool IsActive { get; set; }
        public bool IsHeadOfDepartment { get; set; }

        public virtual EmpTasks? EmpTasks { get; set; }
        public virtual Leader? Leader { get; set; }
        public virtual ICollection<JoinProject>? JoinProjects { get; set; }
        public virtual ICollection<History>? Histories { get; set; }
        public virtual ICollection<RoleUser>? RoleUsers { get; set; }
        public virtual ICollection<Post>? Posts { get; set; }
        public ICollection<Comment>? Comments { get; set; }


    }
}

