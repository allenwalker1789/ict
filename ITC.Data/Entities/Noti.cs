﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ITC.Data.Entities
{
    public class Noti
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string? Content { get; set; }
        public DateTime time { get; set; }

        [ForeignKey("StaffId")]
        public Guid StfId { get; set; }

        [ForeignKey("StId")]
        public Guid StId { get; set; }

        [ForeignKey("CommentId")]
        public Guid CommentId { get; set; }

        public virtual Staff? Staffs { get; set; }
        public virtual Student? Student { get; set; }
        public virtual Comment? Comment { get; set; }

    }
}
