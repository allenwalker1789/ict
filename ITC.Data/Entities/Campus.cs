﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.Data.Entities
{
    public class Campus
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string? Name { get; set; }
        public string? Address { get; set; }
        public bool IsActive { get; set; }

        [ForeignKey("AvatarId")]
        public Guid AvatarId { get; set; }

        public virtual Course? Course { get; set; }
        public ICollection<Avatar>? Avatar { get; set; }
    }
}
