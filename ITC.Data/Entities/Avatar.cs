﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.Data.Entities
{
    public class Avatar
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string? Username { get; set; }
        public string? Password { get; set; }
        public string? FullName { get; set; }
        public string? StaffNo { get; set; }
        public DateTime BirthDay { get; set; }
        public string? PhoneNumber { get; set; }
        public string? Address { get; set; }
        public string? Gender { get; set; }
        public bool IsActive { get; set; }


        [ForeignKey("CampusId")]
        public Guid CampusId { get; set; }

        public virtual Campus? Campuses { get; set; }
        public virtual ICollection<BankInfo>? BankInfos { get; set; }
    }
}
