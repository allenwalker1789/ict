﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ITC.Data.Entities
{
    public class BankInfo
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string? PaymentTo { get; set; }
        public string? BankAccountBeneficiary { get; set; }
        public string? BankAccountNumber { get; set; }
        public string? BankName { get; set; }
        public string? SwiftCode { get; set; }
        public string? BankBranch { get; set; }
        public string? BankAddress { get; set; }
        public bool IsActive { get; set; }


        [ForeignKey("AvatarId")]
        public Guid AvatarId { get; set; }

        public virtual Avatar? Avatar { get; set; }
    }
}
