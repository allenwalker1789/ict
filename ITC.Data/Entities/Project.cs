﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ITC.Data.Entities
{
    public class Project
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string? ProjectName { get; set; }
        public string? Description { get; set; }
        public string? TotalStudent { get; set; }
        public string? MyProperty { get; set; }
        public DateTime Time { get; set; }
        public DateTime MountOfTime { get; set; }
        public DateTime DateCreate { get; set; }

        [ForeignKey("LeaderId")]
        public Guid LeaderId { get; set; }
        [ForeignKey("StatusId")]
        public Guid StatusId { get; set; }
        [ForeignKey("CourseId")]
        public Guid CourseId { get; set; }

        public virtual Leader? Leader { get; set; }
        public virtual Status? Status { get; set; }
        public virtual Course? Course { get; set; }
        public ICollection<Partner>? Partners { get; set; }
        public ICollection<History>? Histories { get; set; }
        public ICollection<JoinProject>? JoinProjects { get; set; }
        public ICollection<StudentJoinProject>? StudentJoinProjects { get; set; }
        public ICollection<EmpTasks>? EmpTasks { get; set; }


    }
}
