﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.Data.Entities
{
    public class Student
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string? FirstName { get; set; }
        public string? PhoneNumber { get; set; }
        public string? Address { get; set; }
        public string? StudentNo { get; set; }
        public string? Gender { get; set; }
        public DateTime BirthDay { get; set; }
        public DateTime SchoolYear { get; set; }
        public bool IsActive { get; set; }


        [ForeignKey("MajorId")]
        public Guid MajorId { get; set; }
        [ForeignKey("RegistrationId")]
        public Guid RegistrationId { get; set; }


        public virtual Major? Major { get; set; }
        public virtual Registration? Registration { get; set; }
        public ICollection<GradeReport>? GradeReports { get; set; }
        public ICollection<StudentJoinProject>? StudentJoinProjects { get; set; }
        public ICollection<Noti>? Notis { get; set; }
        public ICollection<Comment>? Comments { get; set; }
    }
}
