﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.Data.Entities
{
    public class History
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string? Name { get; set; }
        public DateTime TimeUpdate { get; set; } = DateTime.Now;

        [ForeignKey("PartnerId")]
        public Guid PartnerId { get; set; }

        [ForeignKey("ProjectId")]
        public Guid PrjId { get; set; }

        [ForeignKey("StaffId")]
        public Guid StfId { get; set; }

        public virtual Project? Project { get; set; }
        public virtual Staff? Staffs { get; set; }
        public virtual Avatar? Avatar { get; set; }
    }
}
