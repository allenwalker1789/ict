﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.Data.Entities
{
    public class Comment
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string? Content { get; set; }
        public DateTime TimeComment { get; set; }
        public bool IsActive { get; set; }

        [ForeignKey("StaffId")]
        public Guid StfId { get; set; }
        [ForeignKey("StudentId")]
        public Guid StId { get; set; }

        public virtual Staff? Staffs { get; set; }
        public virtual Student? Student { get; set; }
        public ICollection<Noti>? Notis { get; set; }
    }
}
