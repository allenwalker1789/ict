﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITC.Data.Entities
{
    public class JoinProject
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [ForeignKey("ProjectId")]
        public Guid PrjId { get; set; }

        [ForeignKey("StaffId")]
        public Guid StfId { get; set; }

        public virtual Project? Project { get; set; }
        public virtual Staff? Staffs { get; set; }
    }
}
